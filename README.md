#和顺公益基金会后台管理的前端工程

# *安装

安装过程中，可能会出现安装过慢、报错等情况，请尝试以下方式：

# *克隆项目
git clone 项目地址

# *安装依赖

## 1 安装cnpm
npm install -g cnpm --registry=https://registry.npm.taobao.org
## 2 安装依赖
cnpm install

### 中途报错 出现webpack-cli 版本问题
webpack -v 命令检查当前版本 

尝试通过 cnpm install webpack -g 安装

# *启动服务
npm run dev

启动完成后会自动打开浏览器访问 http://localhost:8001，你看到下面的页面就代表成功了。

- 前端工程 基于vue、element-ui构建开发，实现
- 前后端分离，通过token进行数据交互，可独立部署
- 主题定制，通过scss变量统一一站式定制
- 动态菜单，通过菜单管理统一管理访问路由
- 数据切换，通过mock配置对接口数据／mock模拟数据进行切换
- 发布时，可动态配置CDN静态资源／切换新旧版本
- 演示地址：http://fast.demo.renren.io(账号密码：admin/admin)

img[]()
# 说明文档

```
开发环境:修改后台的请求地址
  hs-fund-vue/static/config/index.js
  // api接口请求地址
  window.SITE_CONFIG['baseUrl'] = 'http://localhost:8080/hs-fund-api';

会被统一到 src/utils/httpRequest.js 的方法上做代理和非代理的路径判断
```

# 更新日志
```

```

